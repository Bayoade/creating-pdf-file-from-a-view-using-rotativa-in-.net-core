﻿using PDFPrinterDemo.Core.IRepository;
using PDFPrinterDemo.Core.IService;
using PDFPrinterDemo.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PDFPrinterDemo.AppService.Services
{
    public class EmployeeInfoService : IEmployeeInfoService
    {
        private readonly IEmployeeInfoRepository _employeeInfoRepository;
        public EmployeeInfoService(IEmployeeInfoRepository employeeInfoRepository)
        {
            _employeeInfoRepository = employeeInfoRepository;
        }

        public Task CreateEmployeeInfoAsync(EmployeeInfo employeeInfo)
        {
            return _employeeInfoRepository.CreateEmployeeInfoAsync(employeeInfo);
        }

        public Task DeleteEmployeeInfoAsync(int id)
        {
            return _employeeInfoRepository.DeleteEmployeeInfoAsync(id);
        }

        public Task<IList<EmployeeInfo>> GetAllEmployeeInfos()
        {
            return _employeeInfoRepository.GetAllEmployeeInfos();
        }

        public Task<EmployeeInfo> GetEmployee(int id)
        {
            return _employeeInfoRepository.GetEmployee(id);
        }

        public Task UpdateEmployeeInfoAsync(EmployeeInfo employeeInfo)
        {
            return _employeeInfoRepository.UpdateEmployeeInfoAsync(employeeInfo);
        }
    }
}
