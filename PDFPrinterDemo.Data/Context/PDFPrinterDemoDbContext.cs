﻿using Microsoft.EntityFrameworkCore;
using PDFPrinterDemo.Core.Models;

namespace PDFPrinterDemo.Data.Context
{
    public class PDFPrinterDemoDbContext : DbContext
    {
        public PDFPrinterDemoDbContext(DbContextOptions<PDFPrinterDemoDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasDefaultSchema("dbo");

            var empInfoModelBuilder = builder.Entity<EmployeeInfo>().ToTable("EmployeeInfos");

            empInfoModelBuilder.HasKey(x => x.Id);

            empInfoModelBuilder.Property(b => b.DA).ValueGeneratedOnAddOrUpdate();
            empInfoModelBuilder.Property(b => b.GrossSalary).ValueGeneratedOnAddOrUpdate();
            empInfoModelBuilder.Property(b => b.HRA).ValueGeneratedOnAddOrUpdate();
            empInfoModelBuilder.Property(b => b.NetSalary).ValueGeneratedOnAddOrUpdate();
            empInfoModelBuilder.Property(b => b.TA).ValueGeneratedOnAddOrUpdate();
            empInfoModelBuilder.Property(b => b.TDS).ValueGeneratedOnAddOrUpdate();
        }

        internal DbSet<EmployeeInfo> EmployeeInfos { get; set; }
    }
}
