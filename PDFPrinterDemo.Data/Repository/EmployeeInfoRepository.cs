﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PDFPrinterDemo.Core.IRepository;
using PDFPrinterDemo.Core.Models;
using PDFPrinterDemo.Data.Context;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PDFPrinterDemo.Data.Repository
{
    public class EmployeeInfoRepository : IEmployeeInfoRepository
    {
        private readonly PDFPrinterDemoDbContext _dbContext;
        public EmployeeInfoRepository(PDFPrinterDemoDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public Task CreateEmployeeInfoAsync(EmployeeInfo employeeInfo)
        {
            _dbContext.EmployeeInfos.Add(employeeInfo);

            return _dbContext.SaveChangesAsync();
        }

        public Task DeleteEmployeeInfoAsync(int id)
        {
            var empInfo = new EmployeeInfo { Id = id };
            _dbContext.EmployeeInfos.Attach(empInfo);

            _dbContext.EmployeeInfos.Remove(empInfo);

            return _dbContext.SaveChangesAsync();

        }

        public async Task<IList<EmployeeInfo>> GetAllEmployeeInfos()
        {
            return await _dbContext.EmployeeInfos.ToListAsync();
        }

        public Task<EmployeeInfo> GetEmployee(int id)
        {
            return _dbContext.EmployeeInfos.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task UpdateEmployeeInfoAsync(EmployeeInfo employeeInfo)
        {
            var dbEmpInfo = await _dbContext.EmployeeInfos.FirstOrDefaultAsync(x => x.Id == employeeInfo.Id);

            Mapper.Map(employeeInfo, dbEmpInfo);
            _dbContext.EmployeeInfos.Update(dbEmpInfo);

            await _dbContext.SaveChangesAsync();
        }
    }
}
