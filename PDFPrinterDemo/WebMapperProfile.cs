﻿using AutoMapper;
using PDFPrinterDemo.Core.Models;
using PDFPrinterDemo.Models;

namespace PDFPrinterDemo
{
    public class WebMapperProfile : Profile
    {
        public WebMapperProfile()
        {
            CreateMap<EmployeeInfo, SaveEmployeeInfo>()
                .ReverseMap()
                .ForMember(x => x.DA, y => y.Ignore())
                .ForMember(x => x.GrossSalary, y => y.Ignore())
                .ForMember(x => x.HRA, y => y.Ignore())
                .ForMember(x => x.GrossSalary, y => y.Ignore())
                .ForMember(x => x.TA, y => y.Ignore())
                .ForMember(x => x.TDS, y => y.Ignore());
        }
    }
}
