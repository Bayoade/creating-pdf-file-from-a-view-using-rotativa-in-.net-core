﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using PDFPrinterDemo.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PDFPrinterDemo.Bootstrap
{
    public static class ConfigureAutoMapper
    {
        public static void AddAutoMapperProfiles(this IServiceCollection services)
        {
            Mapper.Initialize(conf =>
            {
                conf.AddProfile<WebMapperProfile>();
                conf.AddProfile<DataMapperProfile>();
            });
        }
    }
}
