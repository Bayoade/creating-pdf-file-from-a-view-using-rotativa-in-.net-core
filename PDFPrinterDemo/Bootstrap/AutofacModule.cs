﻿using Autofac;
using PDFPrinterDemo.AppService.Services;
using PDFPrinterDemo.Core.IRepository;
using PDFPrinterDemo.Core.IService;
using PDFPrinterDemo.Data.Repository;

namespace PDFPrinterDemo.Bootstrap
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EmployeeInfoRepository>()
                .As<IEmployeeInfoRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<EmployeeInfoService>()
                .As<IEmployeeInfoService>()
                .InstancePerLifetimeScope();
        }
    }
}
