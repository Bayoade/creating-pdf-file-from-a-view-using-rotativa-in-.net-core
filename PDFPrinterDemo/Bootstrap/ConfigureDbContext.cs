﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PDFPrinterDemo.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PDFPrinterDemo.Bootstrap
{
    public static class ConfigureDbContext
    {
        public static void AddDbContextConfiguration(this IServiceCollection services)
        {
            services.AddDbContext<PDFPrinterDemoDbContext>(config =>
            {
                config.UseSqlServer(Ioc.Configuration.GetConnectionString("PDFPrinterDemoConnectionKey"));
            });
        }
    }
}
