﻿using Microsoft.Extensions.Configuration;

namespace PDFPrinterDemo.Bootstrap
{
    public static class Ioc
    {
        public static IConfiguration Configuration { get; set; }
    }
}
