﻿namespace PDFPrinterDemo.Models
{
    public class SaveEmployeeInfo
    {
        public int Id { get; set; }

        public string EmployeeName { get; set; }

        public int Salary { get; set; }

        public string DeptName { get; set; }

        public string Designation { get; set; }
    }
}
