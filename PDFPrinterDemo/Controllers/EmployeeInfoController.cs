﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PDFPrinterDemo.Core.IService;
using PDFPrinterDemo.Core.Models;
using PDFPrinterDemo.Models;
using Rotativa.AspNetCore;
using System;
using System.Threading.Tasks;

namespace PDFPrinterDemo.Controllers
{
    public class EmployeeInfoController : Controller
    {
        private readonly IEmployeeInfoService _employeeInfoService;

        public EmployeeInfoController(IEmployeeInfoService employeeInfoService)
        {
            _employeeInfoService = employeeInfoService;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(SaveEmployeeInfo saveEmployeeInfo)
        {
            var empInfo = Mapper.Map<EmployeeInfo>(saveEmployeeInfo);

            await _employeeInfoService.CreateEmployeeInfoAsync(empInfo);

            return RedirectToAction("Index");
        }

        public IActionResult Edit()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Edit(SaveEmployeeInfo saveEmployeeInfo)
        {
            var empInfo = Mapper.Map<EmployeeInfo>(saveEmployeeInfo);

            await _employeeInfoService.UpdateEmployeeInfoAsync(empInfo);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Index()
        {
            var empInfos = await _employeeInfoService.GetAllEmployeeInfos();
            return View(empInfos);
        }

        public async Task<IActionResult> PrintAllReport()
        {
            var allEmployee = await _employeeInfoService.GetAllEmployeeInfos();

            var report = new ViewAsPdf
            {
                ViewName = "Index",
                Model = allEmployee,
                FileName = "AllReportRecords.pdf",
                PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                PageMargins = { Left = 20, Bottom = 20, Right = 20, Top = 20 },
                CustomSwitches = "--footer-center \"  Created Date: " +
                    DateTime.Now.Date.ToString("dd/MM/yyyy") + "  Page: [page]/[toPage]\"" +
                    " --footer-line --footer-font-size \"12\" --footer-spacing 1 --footer-font-name \"Segoe UI\""
            };
            return report;
        }

        public async Task<IActionResult> IndexById(int id)
        {
            var emp = await _employeeInfoService.GetEmployee(id);
            return View(emp);
        }

        public async Task<IActionResult> PrintSalarySlip(int id)
        {
            var employee = await _employeeInfoService.GetEmployee(id);

            var report = new ViewAsPdf
            {
                ViewName = "IndexById",
                Model = employee,
                FileName = "EmployeeReportRecords.pdf",
                PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                PageMargins = { Left = 20, Bottom = 20, Right = 20, Top = 20 },
                CustomSwitches = "--footer-center \"  Created Date: " +
                    DateTime.Now.Date.ToString("dd/MM/yyyy") + "  Page: [page]/[toPage]\"" +
                    " --footer-line --footer-font-size \"12\" --footer-spacing 1 --footer-font-name \"Segoe UI\""
            };

            return report;
        }
    }
}