﻿using PDFPrinterDemo.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PDFPrinterDemo.Core.IService
{
    public interface IEmployeeInfoService
    {
        Task CreateEmployeeInfoAsync(EmployeeInfo employeeInfo);

        Task UpdateEmployeeInfoAsync(EmployeeInfo employeeInfo);

        Task DeleteEmployeeInfoAsync(int id);

        Task<IList<EmployeeInfo>> GetAllEmployeeInfos();

        Task<EmployeeInfo> GetEmployee(int id);
    }
}
