﻿namespace PDFPrinterDemo.Core.Models
{
    public class EmployeeInfo
    {
        public int Id { get; set; }

        public string EmployeeName { get; set; }

        public int Salary { get; set; }

        public string DeptName { get; set; }

        public string Designation { get; set; }

        public decimal HRA { get; private set; }

        public decimal TA { get; private set; }

        public decimal DA { get; private set; }

        public decimal GrossSalary { get; private set; }

        public decimal TDS { get; private set; }

        public decimal NetSalary { get; private set; }
    }

}
